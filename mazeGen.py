#!/usr/bin/python
import sys
import os.path
from random import randint
import listStuff

class Tools:
	size = 5
	sectorSize = 100
	maxSectors = 20
	maxRecursion = 3000
	arrs =	[[1,2,3,4],[1,2,4,3],[1,3,2,4],[1,3,4,2],[1,4,3,2],[1,4,2,3],[2,1,3,4],[2,1,4,3],[2,3,1,4],[2,3,4,1],[2,4,3,1],[2,4,1,3],[3,2,1,4],[3,2,4,1],[3,1,2,4],[3,1,4,2],[3,4,1,2],[3,4,2,1],[4,2,3,1],[4,2,1,3],[4,3,2,1],[4,3,1,2],[4,1,3,2],[4,1,2,3]]
	directory = ""
	stackFiles = ""

	@staticmethod
	def getRand():
		return Tools.arrs[randint(0,23)]



class MyStack(object):

	def __init__(self):
		self.index = 1
		self.fileIndex = 0
		self.rec = [False] * Tools.maxRecursion
		self.rec[0] = (2,2)

	def push(self, p):
		self.checkCapacity()
		self.rec[self.index] = p
		self.index += 1

	def getIndex(self):
		return self.index

	def pop(self):
		self.index -= 1
		if self.index == -1:
			if self.fileIndex > 0:
				self.fillUp()
				self.index = Tools.maxRecursion - 1
			else:
				return None
		return self.rec[self.index]
		self.checkCapacity()

	def checkCapacity(self):
		if self.index >= Tools.maxRecursion:
			self.flush()
			self.index = 0
		elif self.index < 0:
			self.fillUp()
			self.index = Tools.maxRecursion - 1

	def flush(self):
		f = open(Tools.stackFiles + "File_" + str(self.fileIndex) + ".stack", 'w')
		for a in self.rec:
			f.write(str(a[0]) + ";" + str(a[1]) + "\n")
		self.fileIndex += 1

	def fillUp(self):
		self.fileIndex -= 1
		lines = [line.strip() for line in open(Tools.stackFiles + "File_" + str(self.fileIndex) + ".stack", 'r')]
		i = 0
		for line in lines:
			a = line.split(';')
			self.rec[i] = (int(a[0]),int(a[1]))
			i += 1

class MazeManager(listStuff.LinkedList):

	def startUpMethod(self):
		self.currentX = -1
		self.currentY = -1
		self.currentSector = None
		self.counter = 0
		self.target = Tools.size*Tools.sectorSize/2
		self.percentage = -1


	def toCurrent(self, x, y):
		if(not(x == self.currentX and y == self.currentY)):
			i = self.getIndexByKey((x,y))
			if i >= 0:
				temp = self.popFrom(i)
			else:
				if self.length >= Tools.maxSectors:
					(self.popFromTail()).save()
				temp = Sector()
				temp.load(x,y)
			self.currentSector = temp
			self.pushToHead(self.currentSector)
			self.currentX = self.currentSector.key[0]
			self.currentY = self.currentSector.key[1]
		
	def isFree(self, xa, ya):
		x = xa / Tools.sectorSize
		y = ya / Tools.sectorSize
		xr = xa % Tools.sectorSize
		yr = ya % Tools.sectorSize
		self.toCurrent(x,y)
		return self.currentSector.isFree(xr, yr)
	
	def setTaken(self, xa, ya):
		x = xa / Tools.sectorSize
		y = ya / Tools.sectorSize
		xr = xa % Tools.sectorSize
		yr = ya % Tools.sectorSize
		self.toCurrent(x,y)
		self.currentSector.setFree(xr, yr, False)
		self.currentSector.save()
		self.counter += 1
		if(self.target*100/self.count != self.percentage):
			print "Progress: " + str(self.percentage) + "%"



class Sector(listStuff.ListItem):

	def isFree(self, xr, yr):
		if((xr == 0 and self.key[0] == 0) or (xr == Tools.sectorSize-1 and self.key[0] == Tools.size-1) or (yr == 0 and self.key[1] == 0) or (yr == Tools.sectorSize-1 and self.key[1] == Tools.size-1)):
			return False
		return self.sec[yr][xr]

	def setFree(self, xr, yr, freedom):
		self.sec[yr][xr] = freedom

	def load(self, x, y):
		self.key = (x,y)
		if os.path.isfile(self.getLink()) :
			self.sec = self.loadFile()
			self.save()
		else :
			self.sec = self.getEmpty()
			self.save()

	def save(self):
		f = open(self.getLink(),'w')
		res = []
		for line in self.sec:
			temp = []
			for c in line:
				if c :
					temp.append('0')
				else:
					temp.append('1')
			res.append(temp)
		res = [''.join(line) for line in res]
		for line in res:
			f.write(line + "\n")
		f.close() 

	def getLink(self):
		return Tools.directory + str(self.key[0]) + " - " + str(self.key[1]) + ".sec"

	def loadFile(self):
		lines = [line.strip() for line in open(self.getLink())]
		arr = []
		for line in lines:
			arr.append([bool(c == '0') for c in list(line)])
		return arr

	@staticmethod
	def getEmpty():
		arr = []
		for a in range(Tools.sectorSize):
			temp = []
			for b in range(Tools.sectorSize):
				temp.append(True)
			arr.append(temp)
		return arr


class MazeGen(object):
	def __init__(self):
		print "Starting.."
		self.stack = MyStack()
		self.manager = MazeManager()
		self.manager.startUpMethod()


	def canGoTo(self, xa, ya, way):
		if self.manager.isFree(xa, ya):
			if(way == 1):
				return (self.manager.isFree(xa-1, ya) and self.manager.isFree(xa+1, ya) and self.manager.isFree(xa, ya-1) and self.manager.isFree(xa-1, ya-1) and self.manager.isFree(xa+1, ya-1))
			if(way == 2):
				return (self.manager.isFree(xa, ya-1) and self.manager.isFree(xa, ya+1) and self.manager.isFree(xa+1, ya) and self.manager.isFree(xa+1, ya+1) and self.manager.isFree(xa+1, ya-1))
			if(way == 3):
				return (self.manager.isFree(xa-1, ya) and self.manager.isFree(xa+1, ya) and self.manager.isFree(xa, ya+1) and self.manager.isFree(xa-1, ya+1) and self.manager.isFree(xa+1, ya+1))
			if(way == 4):
				return (self.manager.isFree(xa, ya+1) and self.manager.isFree(xa, ya-1) and self.manager.isFree(xa-1, ya+1) and self.manager.isFree(xa-1, ya-1) and self.manager.isFree(xa-1, ya))
		return False

	def createMaze(self):
		coords = self.stack.pop()
		while True:
			arr = Tools.getRand()
			gotSomewhere = False
			for i in arr:
				if i == 1:
					if self.canGoTo(coords[0], coords[1] - 1, 1):
						self.manager.setTaken(coords[0], coords[1] - 1)
						self.stack.push(coords)
						coords = (coords[0], coords[1] - 1)
						gotSomewhere = True
						break
				if i == 2:
					if self.canGoTo(coords[0] + 1, coords[1], 2):
						self.manager.setTaken(coords[0] + 1, coords[1])
						self.stack.push(coords)
						coords = (coords[0] + 1, coords[1])
						gotSomewhere = True
						break
				if i == 3:
					if self.canGoTo(coords[0], coords[1] + 1, 3):
						self.manager.setTaken(coords[0], coords[1] + 1)
						self.stack.push(coords)
						coords = (coords[0], coords[1] + 1)
						gotSomewhere = True
						break
				if i == 4:
					if self.canGoTo(coords[0] - 1, coords[1], 4):
						self.manager.setTaken(coords[0] - 1, coords[1])
						self.stack.push(coords)
						coords = (coords[0] - 1, coords[1])
						gotSomewhere = True
						break
			if not gotSomewhere:
				coords = self.stack.pop()
				if(coords == None):
					break


Tools.directory = sys.argv[1]
Tools.stackFiles = sys.argv[2]
Tools.size = int(sys.argv[3])
Tools.sectorSize = int(sys.argv[4])
Tools.maxSectors = int(sys.argv[5])
Tools.maxRecursion = int(sys.argv[6])

mazeGen = MazeGen()
mazeGen.createMaze()
