class ListItem(object):

	def __init__(self):
		self.before = None
		self.after = None
		self.key = None

	def getBefore(self):
		return self.before

	def getAfter(self):
		return self.after

	def setBefore(self, s):
		self.before = s

	def setAfter(self, s):
		self.after = s

	def getKey(self):
		return self.key

class LinkedList(object):

	def __init__(self):
		self.first = None
		self.last = None
		self.length = 0

	def getIndexByKey(self, key):
		if self.length == 0:
			return -1
		temp = self.first
		i = 0
		while temp != None:
			if temp.getKey() == key:
				return i
			i += 1
			temp = temp.getAfter()


	def popFrom(self, i):
		if self.length == 0:
			return None
		elif i == 0:
			return self.popFromHead()
		elif i == self.length - 1:
			return self.popFromTail()
		elif i < self.length and i >= 0:
			temp = self.first
			while i > 0:
				temp = temp.getAfter()
				i -= 1
			temp.getAfter.setBefore(temp.getBefore())
			temp.getBefore.setAfter(temp.getAfter())
			temp.setAfter(None)
			temp.setBefore(None)
			self.length -= 1
			return temp
		else:
			return None

	def popFromTail(self):
		if self.length == 0:
			return None
		elif self.length == 1:
			self.length = 0
			self.last = None
			temp = self.first
			self.first = None
			return temp
		else:
			self.length -= 1
			temp = self.last
			self.last = self.last.getBefore()
			self.last.setAfter(None)

	def popFromHead(self):
		if self.length == 0:
			return None
		elif self.length == 1:
			self.length = 0
			self.last = None
			temp = self.first
			self.first = None
			return temp
		else:
			self.length -= 1
			temp = self.first
			self.first = self.first.getAfter()
			self.first.setBefore(None)

	def pushToTail(self, a):
		if self.last == None:
			self.first = a
			self.last = a
			lenght = 1
		else:
			a.setBefore(self.last)
			self.last.setAfter(a)
			self.last = a
			self.length += 1

	def pushToHead(self, a):
		if self.first == None:
			self.first = a
			self.last = a
			lenght = 1
		else:
			a.setAfter(self.first)
			self.first.setBefore(a)
			self.first = a
			self.length += 1

