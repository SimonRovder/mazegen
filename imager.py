import Image
import sys

source = sys.argv[1]
dims = int(sys.argv[2])
size = int(sys.argv[3])

img = Image.new( 'RGB', (dims*size,dims*size), "black") # create a new black image
pixels = img.load() # create the pixel map


for y in range(dims):
	for x in range(dims):
		scanner = [line.strip() for line in open(source + str(x) + " - " + str(y) + ".sec", 'r')]
		for yIndex, row in enumerate(scanner):
			for xIndex, c in enumerate(row):
				if(c == '1'):
					pixels[xIndex + size*x,yIndex + size*y] = (0, 255, 0)
				else:
					pixels[xIndex + size*x,yIndex + size*y] = (50, 0, 0)

img.show()
img.save("imagetest.BMP", "BMP")