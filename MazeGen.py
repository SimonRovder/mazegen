#!/usr/bin/python
import sys
import os.path
from random import randint
from ListStuff import *

class Tools:
	size = 5
	sectorSize = 32
	maxSectors = 20
	maxRecursion = 30
	arrs =	[[1,2,3,4],[1,2,4,3],[1,3,2,4],[1,3,4,2],[1,4,3,2],[1,4,2,3],[2,1,3,4],[2,1,4,3],[2,3,1,4],[2,3,4,1],[2,4,3,1],[2,4,1,3],[3,2,1,4],[3,2,4,1],[3,1,2,4],[3,1,4,2],[3,4,1,2],[3,4,2,1],[4,2,3,1],[4,2,1,3],[4,3,2,1],[4,3,1,2],[4,1,3,2],[4,1,2,3]]
	direcotry = ""
	stackFiles = ""

	@staticmethod
	def getRand():
		return Tools.arrs[randint(0,23)]

class MyStack(object):
	def __init__(self):
		self.index = 1
		self.fileIndex = 0
		self.rec = [False] * Tools.maxRecursion
		self.rec[0] = (1,1)

	def push(self, p):
		self.checkCapacity()
		self.rec[self.index] = p
		self.index += 1

	def pop(self):
		self.index -= 1
		if self.index == -1:
			if self.fileIndex > 0:
				self.fillUp()
				self.index = Tools.maxRecursion - 1
			else:
				return None
		return self.rec[self.index]
		self.checkCapacity()

	def checkCapacity(self):
		if self.index >= Tools.maxRecursion:
			self.flush()
			self.index = 0
		elif self.index < 0:
			self.fillUp();
			self.index = Tools.maxRecursion - 1

	def flush(self):
		f = open(Tools.stackFiles + "File_" + self.fileIndex + ".stack", 'w')
		for a in self.rec:
			f.write(a[0] + ";" + a[1])
		self.fileIndex =+ 1

	def fillUp(self):
		self.fileIndex -= 1
		lines = [line.strip() for line in open(Tools.stackFiles + "File_" + self.fileIndex + ".stack", 'r')]
		i = 0
		for line in lines:
			a = line.split(';')
			self.rec[i] = (a[0],a[1])
			i += 1

class MazeManager(LinkedList):
	def __init__(self):
		self.currentX = -1
		self.currentY = -1
		self.currentSector = None
		print("Creating MazeManager.")


	def toCurrent(self, x, y):
		if(not(x == self.currentX and y == self.currentY)):
			i = self.getIndexByKey((x,y))
			if i >= 0:
				temp = self.popFrom(i)
			else:
				if self.lenght >= Tools.maxSectors:
					(self.popFromTail()).save()
				temp = Sector()
				temp.load(x,y)
			self.currentSector = temp
			self.pushToHead(self.currentSector)
			self.currentX = self.currentSector.key[0]
			self.currentY = self.currentSector.key[1]
		
	def isFree(self, xa, ya):
		x = xa / Tools.sectorSize
		y = ya / Tools.sectorSize
		xr = xa % Tools.sectorSize
		yr = ya % Tools.sectorSize
		self.toCurrent(x,y)
		return self.currentSector.isFree(xr, yr)
	
	def setTaken(self, xa, ya):
		x = xa / Tools.sectorSize
		y = ya / Tools.sectorSize
		xr = xa % Tools.sectorSize
		yr = ya % Tools.sectorSize
		self.toCurrent(x,y)
		return self.currentSector.setFree(xr, yr, False)



class Sector(ListItem):

	def isFree(self, xr, yr):
		if((xr == 0 and self.key[0] == 0) or (xr == Tools.sectorSize-1 and x == Tools.size-1) or (yr == 0 and self.key[1] == 0) or (yr == Tools.sectorSize-1 and y == Tools.size-1)):
			return false
		return self.sec[yr][xr]

	def setFree(self, xr, yr, freedom):
		self.sec[yr][xr] = freedom

	def load(self, x, y):
		self.key = (x,y)
		if os.path.isfile(self.getLink()) :
			self.sec = self.loadFile()
			self.save()
		else :
			self.sec = self.getEmpty()
			self.save()

	def save(self):
		f = open(self.getLink(),'w')
		res = []
		for line in self.sec:
			temp = []
			for c in line:
				if c :
					temp.append('0')
				else:
					temp.append('1')
			res.append(temp)
		res = [''.join(line) for line in res]
		for line in res:
			f.write(line + "\n")
		f.close() 

	def getLink(self):
		return Tools.directory + str(self.key[0]) + " - " + str(self.key[1]) + ".sec"

	def loadFile(self):
		lines = [line.strip() for line in open(self.getLink())]
		arr = []
		for line in lines:
			arr.append([bool(c == '0') for c in list(line)])
		return arr

	def __init__(self):
		print("Initializing a sector.")
		ListItem.__init__(self)
		self.load(2,2)

	@staticmethod
	def getEmpty():
		arr = []
		for a in range(Tools.sectorSize):
			temp = []
			for b in range(Tools.sectorSize):
				temp.append(True)
			arr.append(temp)
		return arr


class MazeGen(object):
	def __init__(self):
		print "Starting.."
		self.stack = MyStack()
		self.manager = MazeManager()


	def canGoTo(self, xa, ya, way):
		if self.manager.isFree(xa, ya):
			if(way == 1):
				return (self.manager.isFree(xa-1, ya) and self.manager.isFree(xa+1, ya) and self.manager.isFree(xa, ya-1) and self.manager.isFree(xa-1, ya-1) and self.manager.isFree(xa+1, ya-1))
			if(way == 2):
				return (self.manager.isFree(xa, ya-1) and self.manager.isFree(xa, ya+1) and self.manager.isFree(xa+1, ya) and self.manager.isFree(xa+1, ya+1) and self.manager.isFree(xa+1, ya-1))
			if(way == 3):
				return (self.manager.isFree(xa-1, ya) and self.manager.isFree(xa+1, ya) and self.manager.isFree(xa, ya+1) and self.manager.isFree(xa-1, ya+1) and self.manager.isFree(xa+1, ya+1))
			if(way == 4):
				return (self.manager.isFree(xa, ya+1) and self.manager.isFree(xa, ya-1) and self.manager.isFree(xa-1, ya+1) and self.manager.isFree(xa-1, ya-1) and self.manager.isFree(xa-1, ya))
		return False

	def createMaze(self):
		coords = self.stack.pop()
		while True:
			arr = Tools.getRand()
			gotSomewhere = False
			for i in arr:
				if i == 1:
					if self.canGoTo(coords[0], coords[1] - 1, 1):
						self.manager.setTaken(coords[0], coords[1] - 1)
						self.stack.push(coords)
						coords = (coords[0], coords[1] - 1)
						gotSomewhere = True
						break
				if i == 2:
					if self.canGoTo(coords[0] + 1, coords[1], 2):
						self.manager.setTaken(coords[0] + 1, coords[1])
						self.stack.push(coords)
						coords = (coords[0] + 1, coords[1])
						gotSomewhere = True
						break
				if i == 3:
					if self.canGoTo(coords[0], coords[1] + 1, 3):
						self.manager.setTaken(coords[0], coords[1] + 1)
						self.stack.push(coords)
						coords = (coords[0], coords[1] + 1)
						gotSomewhere = True
						break
				if i == 4:
					if self.canGoTo(coords[0] - 1, coords[1], 4):
						self.manager.setTaken(coords[0] - 1, coords[1])
						self.stack.push(coords)
						coords = (coords[0] - 1, coords[1])
						gotSomewhere = True
						break
			if not gotSomewhere:
				coords = self.manager.pop()
				if(coords == None):
					break


Tools.directory = sys.argv[1]
Tools.stackFiles = sys.argv[2]
mazeGen = MazeGen()
mazeGen.createMaze()